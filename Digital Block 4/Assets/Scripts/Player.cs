﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Animator anim;

    public BoxCollider playerCollider;

    public float moveSpeed;
    public float rotSpeed;
    

    private Ray ray;
    public float rayDistance = 4f;

      

    // Update is called once per frame
    void Update()
    {
        float rotHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 rotate = new Vector3(0f, rotHorizontal, 0f);

        Vector3 move = new Vector3(0f, 0f, moveVertical);

        transform.Rotate(rotate * Time.deltaTime * rotSpeed);

        transform.Translate(move * Time.deltaTime * moveSpeed);

        Animating(moveVertical);

        ray = new Ray(transform.position + new Vector3(0f, playerCollider.center.y, 0f), transform.forward);
        Debug.DrawRay(ray.origin, ray.direction * rayDistance, Color.red);

        RaycastHit hit;
        if(Physics.Raycast(ray, out hit))
        {
            if(hit.collider.gameObject.tag == "Enemy")
            {
                print("¡Estas mirando a un enemigo!");               
            }

            if (hit.collider.gameObject.tag == "Chest")
            {
                hit.collider.gameObject.GetComponent<TreasureChest>().interactable = true;
            }            
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            anim.SetBool("isRunning", true);

        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            anim.SetBool("isRunning", false);
        }

        if (Input.GetMouseButtonDown(0))
        {
            anim.SetLayerWeight(1, 1f);
            anim.SetTrigger("isAttacking");
        }

    }

    private void OnCollisionEnter(Collision collision)
    {      

    }

    void Animating(float vertical)
    {
        bool walking = vertical > 0f;

        anim.SetBool("isWalking", walking);
    }
   

}

